package com.example.hello.alarm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    AlarmManager alarmManager;
    TimePicker timePicker;
    TextView updateText;
    Context context;
    Button setOnButton;
    ArrayList<alarm> alarm_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.context = this;
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        timePicker = findViewById(R.id.timePicker);
        alarm_data = new ArrayList<>();
        final alarm_adapter adapter = new alarm_adapter(this, R.layout.listitem, alarm_data);
        final ListView alarm_list = findViewById(R.id.lvItems);
        alarm_list.setAdapter(adapter);
        final Calendar calendar = Calendar.getInstance();



        setOnButton = (Button) findViewById(R.id.setOn);
        setOnButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                calendar.set(Calendar.HOUR_OF_DAY, timePicker.getHour());
                calendar.set(Calendar.MINUTE, timePicker.getMinute());

                int hour = timePicker.getHour();
                int minute = timePicker.getMinute();



                Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
                i.putExtra(AlarmClock.EXTRA_MESSAGE, "New Alarm");
                i.putExtra(AlarmClock.EXTRA_HOUR, hour);
                i.putExtra(AlarmClock.EXTRA_MINUTES, minute);
                i.putExtra(AlarmClock.EXTRA_SKIP_UI, true);


                startActivity(i);

                alarm newAlarm = new alarm(hour, minute);
                adapter.add(newAlarm);

            }
        });

        };







    public class alarm{
        public int hour;
        public int minute;
        String hour_string;
        String minute_string;
        String time_string;
        public alarm(){
            super();
        }
        public alarm(int hour, int minute){
            super();
            this.hour = hour;
            this.minute = minute;
            this.hour_string = String.valueOf(hour);
            this.minute_string = String.valueOf(minute);

            if (hour > 12){
                hour_string = String.valueOf(hour-12);
            }
            else if (hour < 10){
                hour_string = "0" +hour_string;
            }
            if (minute < 10){
                minute_string = "0" +String.valueOf(minute);
            }
            this.time_string = hour_string + ": " + minute_string;
        }

    }

    public class alarm_adapter extends ArrayAdapter<alarm>{
        public alarm_adapter(Context context, int listViewResource, ArrayList<alarm> alarms) {
            super(context, R.layout.listitem, alarms);
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            final alarm alarm = getItem(position);
            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem, parent, false);
            }
            TextView tvTime = convertView.findViewById(R.id.tvTime);
            Switch switchButton = convertView.findViewById(R.id.switchButton);
            Button removeButton = convertView.findViewById(R.id.remove_button);
            switchButton.setChecked(true);
            removeButton.setOnClickListener(new View.OnClickListener(){
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    assert alarm!= null;
     /*               Intent dismiss = new Intent(AlarmClock.ACTION_DISMISS_ALARM);
                    dismiss.putExtra(AlarmClock.EXTRA_MINUTES, alarm.minute);
                    dismiss.putExtra(AlarmClock.EXTRA_HOUR, alarm.hour);
                    dismiss.putExtra(AlarmClock.EXTRA_ALARM_SEARCH_MODE, AlarmClock.ALARM_SEARCH_MODE_TIME);
                    dismiss.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                    startActivity(dismiss);*/
                    alarm_data.remove(position);
                    notifyDataSetChanged();
                }


            });
            assert alarm != null;
            tvTime.setText(alarm.time_string);
            tvTime.setTextSize(30);
            tvTime.setPadding(300,0,60,0);
            switchButton.setPadding(50,50,50,0);
            tvTime.setHeight(100);
            tvTime.setWidth(100);
            tvTime.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            return convertView;
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
